#ifndef MAINAPPLICATION_H
#define MAINAPPLICATION_H

#include <QWidget>
#include <QPainter>
#include <QImage>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QFileDialog>
#include <QStringListModel>
#include <QLabel>
#include "Polygone.h"

QT_BEGIN_NAMESPACE
namespace Ui { class mainApplication; }
QT_END_NAMESPACE

class mainApplication : public QWidget
{
    Q_OBJECT

public:
    mainApplication(QWidget *parent = nullptr);
    ~mainApplication();

private:
    Ui::mainApplication *ui;
    QImage *mapImage;
    Polygone *polygone;
    bool m_flag = false; // 绘图标志
    bool drag = false; // 拖拽标志
    bool navPoint_flag = false; // 画航点标志
    QStringList points;
    QStringListModel *model;


private slots:
    void on_imageButton_clicked(); //信号槽
    void on_accompBtm_clicked();
    void on_addPointBtm_clicked();
    void on_clearBtn_clicked();
    void on_navigation_clicked();

protected:
    bool eventFilter(QObject *object, QEvent *event);
};
#endif // MAINAPPLICATION_H
