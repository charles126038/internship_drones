#include "Polygone.h"

ostream& operator << (ostream& os, const Polygone& p) {

	os << string(p);
	return os;

}

string Polygone::versString() const
{
	string requete = to_string(Forme::POLYGONE) + "," + to_string(getCouleur()) + "," + to_string(getNombreSommets());
	vector<Vecteur_2D> sommets = getSommets();
	for (Vecteur_2D v : sommets) {
		requete = requete + "," + to_string(v.getX()) + "," + to_string(v.getY());
	}
	return requete;
}

void Polygone::clear(){
    _sommets.clear();
    _nombrePoints = 0;
}

bool Polygone::isConvex(){
    int i, index = 0;
    Vecteur_2D tp = _sommets[0];
    for(i = 1; i < _nombrePoints; i++){ // 寻找第一个凸点
        if(_sommets[i].getY() < tp.getY() || (_sommets[i].getY() == tp.getY() && _sommets[i].getX() < tp.getX())){
            tp = _sommets[i];
            index = i;
        }
    }
    int count = _nombrePoints - 1;
    bool bc[_nombrePoints];
    bc[index] = 1;
    while(count){
        if(multiply(_sommets[(index+1)%_nombrePoints], _sommets[(index+2)%_nombrePoints], _sommets[index]) <= 0) // 如果是顺时针
            bc[(index+1)%_nombrePoints] = 1;
        else
            bc[(index+1)%_nombrePoints] = 0;
        index++;
        count--;
    }
    for(int i = 0; i < _nombrePoints; i++){
        if(!bc[i])
            return false;
    }
    return true;
}

double Polygone::aire() const
{
    double s1, s2;
    s1 = s2 = 0;
    double result = 0;
    if(_nombrePoints < 3)
        return 0;
    for (int i = 0; i < (int)_sommets.size() - 1; i++)
	{
        s1 = s1 + _sommets[i].getX()*_sommets[i+1].getY();
        s2 = s2 + _sommets[i].getY()*_sommets[i+1].getX();
	}
    result = abs((s1 - s2)/2);
	return result;
}
