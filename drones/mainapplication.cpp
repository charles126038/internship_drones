#include "mainapplication.h"
#include "ui_mainapplication.h"
#include "Vecteur_2D.h"

mainApplication::mainApplication(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::mainApplication)
{
    ui->setupUi(this);
    this->setWindowTitle("无人机拍摄路径规划 - TANG Yuhang");
    //ui->mapFond->setFrameStyle(1);
    this->mapImage = new QImage();
    polygone = new Polygone();
    ui->mapFond->scale(1, -1);

    // 将需要经过子控件监控修改的传递下去
    ui->mapFond->setCoordView(ui->viewCoord);
    ui->mapFond->setCoordMap(ui->mapCoord);
    ui->mapFond->setListPoints(&points);
    ui->mapFond->setListView(ui->listView);
    ui->mapFond->setBestAngle(ui->bestAngle);
    ui->mapFond->setRateRebundant(ui->rateRedundant);

    //给画板安装过滤器
    ui->mapFond->installEventFilter(this);

    // 禁用画航点按钮，直到多边形画出来
    ui->navigation->setDisabled(true);
    //connect the button listener event
//    connect(ui->imageButton,SIGNAL(clicked()), this, SLOT(on_imageButton_clicked()), Qt::UniqueConnection); //打开地图按钮动作

//    connect(ui->addPointBtm, SIGNAL(clicked()), this, SLOT(on_addPointBtm_clicked()), Qt::UniqueConnection); // 增加点按钮动作

//    connect(ui->accompBtm, SIGNAL(clicked()), this, SLOT(on_accompBtm_clicked()), Qt::UniqueConnection); // 画多边形按钮动作
}

mainApplication::~mainApplication()
{
    delete mapImage;
    delete ui;
}

// 打开地图（暂时还是图片）
void mainApplication::on_imageButton_clicked()
{
    ui->mapFond->openMap();
}

// 多边形显示在画板
void mainApplication::on_accompBtm_clicked(){
    if(polygone->getNombreSommets() >= 3){
        m_flag = true;
        update();
        ui->navigation->setEnabled(true);// 可以画航点了
    }
}

// 增加点
void mainApplication::on_addPointBtm_clicked(){
    QString xString = ui->latitude->text();
    QString yString = ui->longitude->text();
    Vecteur_2D point = Vecteur_2D(xString.toDouble(), yString.toDouble());
    bool hasEqual = false; // 判断是否有重复的点
    for(int i = 0; i < polygone->getNombreSommets(); i++){
        if(polygone->getSommets()[i] == point){
            hasEqual = true;
            break;
        }
    }
    if(!hasEqual && xString != "" && yString != ""){
        polygone->ajouterSommet(point);
        points << xString + " , " + yString;
        model = new QStringListModel(points);
        ui->listView->setModel(model);
        ui->listView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    }
}

// 清除所有点和多边形
void mainApplication::on_clearBtn_clicked(){
    model = new QStringListModel();
    points.clear();
    ui->listView->setModel(model);
    polygone->clear();
    ui->mapFond->update();
}

//画航点
void mainApplication::on_navigation_clicked(){
    navPoint_flag = true;
    update();
}


// 过滤器
bool mainApplication::eventFilter(QObject *watched, QEvent *event){
    if(watched == ui->mapFond && event->type() == QEvent::Paint){
        if(m_flag) // 如果按钮按下，开始画多边形
            ui->mapFond->drawPolygone(polygone);
        if(navPoint_flag)
            ui->mapFond->drawCoursepoints();
    }
    return QWidget::eventFilter(watched,event);
}


