#include "mainapplication.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    mainApplication w;
    w.show();
    return a.exec();
}
