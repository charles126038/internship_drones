#include "mapController.h"
#include <QSlider>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <math.h>
#include <QGraphicsRectItem>
#include <QTextStream>
#include "navigation_algorithms.h"


mapController::mapController(QWidget *parent):QGraphicsView(parent){
    zoom = 50;
    map = new QImage();
    scene = new QGraphicsScene();
    this->setScene(scene);

    QSlider *slider = new QSlider(this);//新建一个滚动条控制地图的缩放
    slider->setOrientation(Qt::Horizontal);//水平滚动条
    slider->setRange(1,100);//缩放范围为1~100
    slider->setTickInterval(10);//设置滑动间隔
    slider->setValue(50);//设置初始值为50
    connect(slider,SIGNAL(valueChanged(int)),this,SLOT(slotZoom(int)));


}

void mapController::setMap(QImage *m){
    map = m;
}

void mapController::setCoordView(QLabel *l){
    coordView = l;
}

void mapController::setCoordMap(QLabel* l){
    mapCoord = l;
}
void mapController::setListPoints(QStringList *l){
    listPoints = l;
}
void mapController::setListView(QListView* v){
    listView = v;
}

QPointF mapController::sceneToLatLon(QPointF p)
{
    QPointF latLon;
    qreal w = this->sceneRect().width();
    qreal h = this->sceneRect().height();
    qreal lon = y1-(p.y()*abs(y1-y2)/h);
    qreal lat = x1+(p.x()*abs(x1-x2)/w);
    latLon.setX(lat);
    latLon.setY(lon);
    return latLon;
}

QPointF mapController::latLonToScene(QPointF latLon)
{
    QPointF CoordScene;
    qreal w = this->sceneRect().width();
    qreal h = this->sceneRect().height();
    qreal lon = y1-((h/2+latLon.y())*abs(y1-y2)/h);
    qreal lat = x1-((w/2+latLon.x())*abs(x1-x2)/w);
    CoordScene.setX(lat);
    CoordScene.setY(lon);
    return CoordScene;
}

void mapController::slotZoom(int value)
{
    qreal s;
    if(value>zoom)
    {
        s = pow(1.01,(value-zoom));
    }
    else
    {
        s = pow(1/1.01,(zoom-value));
    }
    this->scale(s,s);//缩放功能
    zoom = value;
}

void mapController::mouseMoveEvent(QMouseEvent *event)
{
    QPoint viewPoint = event->pos();
    QPointF scenePoint = mapToScene(viewPoint);
    coordView->setText(QString::number(scenePoint.x())+","+QString::number(scenePoint.y()));
//    sceneCoord->setText(QString::number(scenePoint.x())+","+QString::number(scenePoint.y()));
    QPointF latLon = sceneToLatLon(scenePoint);//获得地图经纬度
    mapCoord->setText(QString::number(latLon.x())+","+QString::number(latLon.y()));



    for(int i = 0; i < (int)point.size(); i++){  // 鼠标碰到点
        if(point.at(i)->rect().contains(this->mapToScene(event->pos()))){
            setCursor(Qt::PointingHandCursor);
            break;
        }
        else
            setCursor(Qt::ArrowCursor);
    }

    if(my_drag){ // 开始拖拽
        // 最佳航向角
        if(navigation != NULL){
            bestAngle->setText(QString::number(navigation->getBestAngle()));
            rateRebundant->setText(QString::number(navigation->getRateRebudant()*100)+"%");
        }

        Vecteur_2D v = Vecteur_2D(this->mapToScene(event->pos()).x(), this->mapToScene(event->pos()).y());
        Polygone *iter = new Polygone(*polygone);
        iter->setSommet(v, drag_ind);
        if(iter->isConvex()){
            polygone->setSommet(v, drag_ind);
            update();

            // 修改父控件，更新拖拽后的坐标
            listPoints->replace(drag_ind, QString::number(polygone->getSommets()[drag_ind].getX(), 'f', 0) + " , "
                                + QString::number(polygone->getSommets()[drag_ind].getY(), 'f', 0));
            QStringListModel  *model = new QStringListModel(*listPoints);
            listView->setModel(model);
            listView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        }

    }
}

/**
 * @brief mapController::mousePressEvent 鼠标按下动作
 * @param event
 */
void mapController::mousePressEvent(QMouseEvent *event){
    for(int i = 0; i < (int)point.size(); i++){  // 鼠标碰到点
        if(point.at(i)->rect().contains(this->mapToScene(event->pos()))){
            my_drag = true;  // 可拖拽
            drag_ind = i;
        }
    }
}

/**
 * @brief mapController::mouseReleaseEvent 鼠标放开，拖拽结束
 * @param event
 */
void mapController::mouseReleaseEvent(QMouseEvent *event){
    my_drag = false;
    drag_ind = -1;
}

/**
 * @brief mapController::openMap 读取地图信息
 * 格式：图片名字位置 左上角经度 左上角纬度 右下角经度 右下角纬度
 */
void mapController::openMap(){
    QString mapName;
    QString fileName = QFileDialog::getOpenFileName(
                this, "open map file",
                "../drones/mapsInfo",
                "MapInfo files (*.mapInfo);");
    if(fileName != "")
    {
        QFile mapFile(fileName);
        int ok = mapFile.open(QIODevice::ReadOnly);
        if(ok){
            QTextStream ts(&mapFile);
                if(!ts.atEnd())
                {
                    ts>>mapName;
                    ts>>x1>>y1>>x2>>y2;
                }
        }
    }
    if(map->load(mapName))
    {
        scene->addPixmap(QPixmap::fromImage(*map));
        this->show();
    }
}

// 画多边形
void mapController::drawPolygone(Polygone *polygone){
    // 重置舞台，清除之前的多边形
    scene->clear();
    scene->addPixmap(QPixmap::fromImage(*map));
    point.clear();

    this->polygone = polygone;
    QGraphicsPolygonItem *item = new QGraphicsPolygonItem;
    QPolygonF points;
    for(int i = 0; i < polygone->getNombreSommets(); i++){
        points.append(QPointF(polygone->getSommets()[i].getX(), polygone->getSommets()[i].getY()));
        QGraphicsEllipseItem *p = new QGraphicsEllipseItem(polygone->getSommets()[i].getX()-4,
                                                               polygone->getSommets()[i].getY()-4, 8, 8);
        p->setBrush(QBrush(QColor(255,132,107,127)));
        scene->addItem(p);
        point.push_back(p);
        p->setFlags(QGraphicsItem::ItemIsMovable
                               | QGraphicsItem::ItemIsSelectable
                               | QGraphicsItem::ItemIsFocusable);
    }
    item->setPolygon(points);
    //item->setFlags(QGraphicsItem::ItemIsMovable
    //                   | QGraphicsItem::ItemIsSelectable
    //                   | QGraphicsItem::ItemIsFocusable);
    item->setBrush(QBrush(QColor(255,190,107,127)));
    scene->addItem(item);
}

void mapController::drawCoursepoints(){
    navigation = new Navigation(polygone);

    navigation->algorithm();

//    for(int i = 0; i < (int)navigation.getPoints().size(); i++){  // 画点
//        QGraphicsEllipseItem *p = new QGraphicsEllipseItem(navigation.getPoints()[i].getX()-3, navigation.getPoints()[i].getY()-3, 6, 6);
//        p->setBrush(QBrush(QColor("#0013A3")));
//        scene->addItem(p);
//    }
    bestAngle->setText(QString::number(navigation->getBestAngle()));
    rateRebundant->setText(QString::number(navigation->getRateRebudant()*100)+"%");

    for(int i = 0; i < (int)navigation->getSegments().size(); i++){ // 画线
        QGraphicsEllipseItem *p1 = new QGraphicsEllipseItem(navigation->getSegments()[i][0].getX()-3, navigation->getSegments()[i][0].getY()-3, 6, 6);
        p1->setBrush(QBrush(QColor("#0013A3")));
        scene->addItem(p1);
        QGraphicsEllipseItem *p2 = new QGraphicsEllipseItem(navigation->getSegments()[i][1].getX()-3, navigation->getSegments()[i][1].getY()-3, 6, 6);
        p2->setBrush(QBrush(QColor("#0013A3")));
        scene->addItem(p2);
        QGraphicsLineItem *line = new QGraphicsLineItem(navigation->getSegments()[i][0].getX(), navigation->getSegments()[i][0].getY(),
                                                        navigation->getSegments()[i][1].getX(), navigation->getSegments()[i][1].getY());
        line->setPen(QPen(QColor("#6B7CFF"),2));
        scene->addItem(line);
    }
}
