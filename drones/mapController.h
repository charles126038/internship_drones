#ifndef MAPCONTROLLER_H
#define MAPCONTROLLER_H
#include <QGraphicsView>
#include <QLabel>
#include <QMouseEvent>
#include <QFileDialog>
#include <vector>
#include <QListView>
#include "Polygone.h"
#include "mainapplication.h"
#include "navigation_algorithms.h"

// 控制地图界面
class mapController : public QGraphicsView{
    Q_OBJECT

private:
    QImage *map;
    qreal zoom;
    double x1, y1, x2, y2;
    QGraphicsScene *scene;

    // 父控件
    QLabel *coordView;
    QLabel *mapCoord;
    QStringList *listPoints;
    QListView *listView;
    QLabel *bestAngle;
    QLabel *rateRebundant;

    vector<QGraphicsEllipseItem*> point;
    Polygone *polygone;
    bool my_drag = false; // 判断是否拖动点
    int drag_ind; // 拖拽的点的位置
    Navigation *navigation;

public:
    mapController(QWidget *parent = 0);
    QPointF sceneToLatLon(QPointF); // 获得地图的经度纬度
    QPointF latLonToScene(QPointF); // 将经纬坐标转换为画框坐标


    void setMap(QImage*);
    void setCoordView(QLabel*);
    void setCoordMap(QLabel*);
    void setListPoints(QStringList*);
    void setListView(QListView*);
    void setBestAngle(QLabel *l) { bestAngle = l; }
    void setRateRebundant(QLabel *l) { rateRebundant = l; }


    void openMap();
    void drawPolygone(Polygone *);
    void drawCoursepoints();


public slots:
    void slotZoom(int);


protected:
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
};

#endif // MAPCONTROLLER_H
