#pragma once

#include "Forme.h"
#include <vector>
#include <math.h>

/**
 * @brief une classe Polygone qui herite de la classe Forme
 * int _nombrePoints
 * vector<Vecteur_2D> _sommets
*/
class Polygone : public Forme {
private:
	int _nombrePoints;
	vector<Vecteur_2D> _sommets;
    /**
     * @brief multiply 得到(sp-op)和(ep-op)的叉积
     * @param sp
     * @param ep
     * @param op
     * @return
     *
    r>0：ep在矢量opsp的逆时针方向；
    r=0：opspep三点共线；
    r<0：ep在矢量opsp的顺时针方向
     */
    double multiply(Vecteur_2D sp, Vecteur_2D ep, Vecteur_2D op){
        return ((sp.getX()-op.getX())*(ep.getY()-op.getY()) - (ep.getX()-op.getX())*(sp.getY()-op.getY()));
    }

public:
	/**
	 * @brief c'est un constructeur de la classe Polygone
	 * @param nombrePoints const int
	 * @param points vecteur<double>
	 * @return 
	*/
    explicit Polygone(const int &nombrePoints, vector<double> points) :Forme(POLYGONE) {
		_nombrePoints = nombrePoints;
		for (int i = 0; i < nombrePoints*2; i = i + 2) {
			_sommets.push_back(Vecteur_2D(points[i], points[i + 1]));
		}
	}

    explicit Polygone():Forme(POLYGONE){ _nombrePoints = 0; }


	/**
	 * @brief c'est un constructeur de la classe Polygone
	 * @param p const Polygone
	 * @return 
	*/
	explicit Polygone(const Polygone& p) :Forme(p),
		_nombrePoints(p._nombrePoints) {
		for (int i = 0; i < _nombrePoints; i++) {
			Vecteur_2D v = Vecteur_2D(p._sommets.at(i));
			_sommets.push_back(v);
		}
	}
	/**
	 * @brief getter getNombreSommets
	 * @return int
	*/
	inline const int getNombreSommets() const { return _nombrePoints; }
	/**
	 * @brief getter getSommets
	 * @return const vector<Vecteur_2D>
	*/
	inline const vector<Vecteur_2D> getSommets() const { return _sommets; }
	/**
	 * @brief setter setNombreSommets
	 * @param nombre const int  
	 * @return 
	*/
	inline const void setNombreSommets(const int &nombre) { _nombrePoints = nombre; }
	/**
	 * @brief setter ajouterSommet
	 * @param v const Vecteur_2D
	 * @return 
	*/
	inline const void ajouterSommet(const Vecteur_2D& v) { _sommets.push_back(v); _nombrePoints++; }
	
	string versString() const;
	/**
     * @brief replace the point at 'pos' with 'v'
	 * @param v const Vecteur_2D
	 * @param pos const int la position
	 * @return 
	*/
	const void setSommet(const Vecteur_2D& v, const int& pos) { _sommets.at(pos) = v; }

    void clear();

    bool isConvex();// 判断多边形是否是凸多边形

	/**
	 * @brief Surface calculee
	 * @return  double
	*/
	virtual double  aire()const;
	
    operator string() const;
	
	friend ostream& operator << (ostream& os, const Polygone& p);
//protected:
//	ostream& afficher(ostream& os) const {
//		os << "Polygone[ nombreSommets= " << _nombrePoints << ", " << endl;
//		os << "\tSommets: " << endl;
//		for (int i = 0; i < _nombrePoints; i++) {
//			os << "\t\t" << _sommets.at(i) << endl;
//		}
//		os << " ]" << endl;
//		return os;
//	}
};


inline Polygone::operator string() const {

	ostringstream os;
	os << "Polygone [ nombreSommets= " << _nombrePoints << ", couleur: " << getCouleur() << ", " << endl;
	os << "\tSommets: " << endl;
	for (int i = 0; i < _nombrePoints; i++) {
		os << "\t\t" << _sommets.at(i) << endl;
	}
	os << "\taire : " << aire() << " ]" << endl;
	return os.str();
}
