#include "navigation_algorithms.h"
#include <fstream>
#include <sstream>
#include <cmath>
#include <QFileDialog>
#include <QtDebug>
#include <QTextStream>

double Navigation::angleToRadian(int angle)
{
    return (angle * PI) / 180;
}

Vector2d Navigation::linearSolution(double a1, double b1, double c1, double a2, double b2, double c2) {
    Matrix2d functions;
    functions(0, 0) = a1;
    functions(0, 1) = b1;
    functions(1, 0) = a2;
    functions(1, 1) = b2;
    Vector2d variableC;
    variableC << -c1, -c2;  // c在等式右边为-c
    Vector2d res = functions.colPivHouseholderQr().solve(variableC);
    return res;
}

double Navigation::heuristicCalculate(vector<vector<Vecteur_2D>> segments){
    double s = 0; // 工作实际面积
    for(int i = 0; i < (int)segments.size(); i++){
        if(i % 2 == 0){ // 奇数线段(工作平行线)
            s = s + radiusDrone*2*sqrt(pow(segments[i][1].getX()-segments[i][0].getX(), 2)
                                        + pow(segments[i][1].getY()-segments[i][0].getY(), 2));
        }
    }
    return abs((s - polygone->aire())/polygone->aire());
}

Navigation::Navigation(Polygone* polygone)
{
    this->polygone = polygone;
    QFile file(":/mapsInfo/droneNavigationSetting.txt");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        qDebug() << "Can't open the file!" << endl;
    QTextStream in(&file);
    QString line = in.readLine();
    if (line != "")
        radiusDrone = line.toDouble();

    while (!line.isNull()) {
        line = in.readLine();
        if(line != "")
            this->courseAngles.push_back(line.toDouble());
    }
}

void Navigation::algorithm()
{
    double heuristic = 1; // 启发值
    for(int i = 0; i < (int)courseAngles.size(); i++){ // 比较每个航向角的多余覆盖率
        vector<vector<Vecteur_2D>> segments = navigationPoints(courseAngles[i]);
        double h = heuristicCalculate(segments);
        if(h < heuristic){
            heuristic = h;
            segmentsNavigation = segments;
            bestAngle = courseAngles[i];
        }
    }
    rateRedundant = heuristic;
}

Vecteur_2D Navigation::originCoordRotation(const Vecteur_2D& v, const int& angle)
{
    int a = angle;
    if (angle > 180) a = angle - 180;

    MatrixXd m(2, 2); // 转换矩阵


    Vecteur_2D newV = Vecteur_2D();
    if (a > 0 && a <= 90) {
        // 逆时针旋转
        m(0, 0) = cos(angleToRadian(90 - a));
        m(1, 0) = -sin(angleToRadian(90 - a));
        m(0, 1) = sin(angleToRadian(90 - a));
        m(1, 1) = cos(angleToRadian(90 - a));

        double Xmin, Xmax;
        Vecteur_2D o = Vecteur_2D();  // 新坐标系原点
        Xmax = polygone->getSommets()[0].getX();
        Xmin = polygone->getSommets()[0].getX();
        for (int i = 0; i < polygone->getNombreSommets(); i++) {
            if (Xmax < polygone->getSommets()[i].getX())
                Xmax = polygone->getSommets()[i].getX();
            if (Xmin > polygone->getSommets()[i].getX())
                Xmin = polygone->getSommets()[i].getX();
        }

        // 新坐标系x轴表达式：y = 1/tan(a)X - Xmax/tan(a)
        o.setX(Xmin);
        o.setY((1 / tan(angleToRadian(a))) * Xmin - Xmax / tan(angleToRadian(a)));

        line(0) = 1 / tan(angleToRadian(a));
        line(1) = -Xmax / tan(angleToRadian(a));

        Vector2d oldCoord;
        oldCoord(0) = v.getX() - o.getX();
        oldCoord(1) = v.getY() - o.getY();

        MatrixXd newCoord = m * oldCoord;
        newV.setX(newCoord(0, 0));
        newV.setY(newCoord(1, 0));
    }
    else {
        // 顺时针旋转
        m(0, 0) = cos(angleToRadian(a - 90));
        m(1, 0) = sin(angleToRadian(a - 90));
        m(0, 1) = -sin(angleToRadian(a - 90));
        m(1, 1) = cos(angleToRadian(a - 90));

        double Ymin, Ymax;
        Vecteur_2D o = Vecteur_2D(); // 新坐标原点
        Ymin = polygone->getSommets()[0].getY();
        Ymax = polygone->getSommets()[0].getY();
        for (int i = 0; i < polygone->getNombreSommets(); i++) {
            if (Ymax < polygone->getSommets()[i].getY())
                Ymax = polygone->getSommets()[i].getY();
            if (Ymin > polygone->getSommets()[i].getY())
                Ymin = polygone->getSommets()[i].getY();
        }

        // 新坐标系x轴表达式: y = -1/tan(180-a)X + Ymin
        o.setX((Ymin - Ymax) * tan(angleToRadian(180 - a)));
        o.setY(Ymax);

        line(0) = -1 / tan(angleToRadian(180 - a));
        line(1) = Ymin;

        Vector2d oldCoord;
        oldCoord(0) = v.getX() - o.getX();
        oldCoord(1) = v.getY() - o.getY();

        MatrixXd newCoord = m * oldCoord;
        newV.setX(newCoord(0, 0));
        newV.setY(newCoord(1, 0));
    }
    return newV;
}

Vecteur_2D Navigation::toOriginCoord(const Vecteur_2D& v, const int& angle)
{
    int a = angle;
    if (angle > 180) a = angle - 180;

    MatrixXd m(2, 2); // 旋转矩阵

    Vecteur_2D oldV = Vecteur_2D();
    if (a > 0 && a <= 90) {
        // 转新坐标时的矩阵的转置矩阵
        m(0, 0) = cos(angleToRadian(90 - a));
        m(1, 0) = sin(angleToRadian(90 - a));
        m(0, 1) = -sin(angleToRadian(90 - a));
        m(1, 1) = cos(angleToRadian(90 - a));

        // 旋转回原本坐标
        MatrixXd newCoord(2, 1);
        newCoord(0, 0) = v.getX();
        newCoord(1, 0) = v.getY();
        MatrixXd oldCoord = m * newCoord;

        double Xmin, Xmax;
        Vecteur_2D o = Vecteur_2D();  // 新坐标系原点
        Xmax = polygone->getSommets()[0].getX();
        Xmin = polygone->getSommets()[0].getX();
        for (int i = 0; i < polygone->getNombreSommets(); i++) {
            if (Xmax < polygone->getSommets()[i].getX())
                Xmax = polygone->getSommets()[i].getX();
            if (Xmin > polygone->getSommets()[i].getX())
                Xmin = polygone->getSommets()[i].getX();
        }

        // 新坐标系x轴表达式：y = 1/tan(a)X - Xmax/tan(a)
        o.setX(Xmin);
        o.setY(1 / tan(angleToRadian(a)) * Xmin - Xmax / tan(angleToRadian(a)));

        // 平移回原坐标
        oldV.setX(oldCoord(0, 0) + o.getX());
        oldV.setY(oldCoord(1, 0) + o.getY());
    }
    else {
        // 转新坐标时的矩阵的转置矩阵
        m(0, 0) = cos(angleToRadian(a - 90));
        m(1, 0) = -sin(angleToRadian(a - 90));
        m(0, 1) = sin(angleToRadian(a - 90));
        m(1, 1) = cos(angleToRadian(a - 90));

        // 旋转回原本坐标系
        MatrixXd newCoord(2, 1);
        newCoord(0, 0) = v.getX();
        newCoord(1, 0) = v.getY();
        MatrixXd oldCoord = m * newCoord;

        double Ymin, Ymax;
        Vecteur_2D o = Vecteur_2D(); // 新坐标原点
        Ymin = polygone->getSommets()[0].getY();
        Ymax = polygone->getSommets()[0].getY();
        for (int i = 0; i < polygone->getNombreSommets(); i++) {
            if (Ymax < polygone->getSommets()[i].getY())
                Ymax = polygone->getSommets()[i].getY();
            if (Ymin > polygone->getSommets()[i].getY())
                Ymin = polygone->getSommets()[i].getY();
        }

        // 新坐标系x轴表达式: y = -1/tan(180-a)X + Ymin
        o.setX((Ymin - Ymax) * tan(angleToRadian(180 - a)));
        o.setY(Ymax);

        // 平移回原坐标
        oldV.setX(oldCoord(0, 0) + o.getX());
        oldV.setY(oldCoord(1, 0) + o.getY());
    }

    return oldV;
}

Vecteur_2D Navigation::intersection(Vector2d line1, Vector2d line2) {
    Vector2d res = linearSolution(line1[0], -1, line1[1], line2[0], -1, line2[1]); // 矩阵求解
    Vecteur_2D result = Vecteur_2D(res(0), res(1));
    return result;
}

vector<vector<Vecteur_2D>> Navigation::navigationPoints(const int& angle) {
    Polygone* newPoly = new Polygone(); // 新坐标系多边形
    vector<vector<Vecteur_2D>> segments;
    points.clear();

    if(polygone->getNombreSommets() == 0)
        return segments;

    for (int i = 0; i < polygone->getNombreSommets(); i++) {
        Vecteur_2D v = polygone->getSommets()[i];
        v = originCoordRotation(v, angle);
        newPoly->ajouterSommet(v);
    }

    double Ymin, Ymax;
    Ymin = newPoly->getSommets()[0].getY();
    Ymax = newPoly->getSommets()[0].getY();
    // 找到多边形最高点与最低点，计算子作业区域数量
    for (int i = 0; i < newPoly->getNombreSommets(); i++) {
        if (Ymax < newPoly->getSommets()[i].getY())
            Ymax = newPoly->getSommets()[i].getY();
        if (Ymin > newPoly->getSommets()[i].getY()){
            Ymin = newPoly->getSommets()[i].getY();
        }
    }
    int n = (Ymax - Ymin) / (radiusDrone * 2) + 1; // 子作业区域数量
    Vector2d iterLine; // 作业区域直线

    // 初始化第一条线
    iterLine[0] = 0;
    iterLine[1] = Ymin - radiusDrone;
    for (int i = 0; i < n; i++) { // 子区域边界线与多边形交点
        iterLine[1] += radiusDrone*2;
        for (int j = 0; j < newPoly->getNombreSommets(); j++) { // 找到与多边形的交点
            Vector2d linePolygon;
            double x1 = newPoly->getSommets()[j].getX();
            double x2, y2;
            if (j != newPoly->getNombreSommets() - 1) {
                x2 = newPoly->getSommets()[j + 1].getX();
                y2 = newPoly->getSommets()[j + 1].getY();
            }
            double y1 = newPoly->getSommets()[j].getY();

            if (j == newPoly->getNombreSommets() - 1) {
                // 多边形最后一条边直线
                linePolygon = linearSolution(x1, 1, -y1,
                    newPoly->getSommets()[0].getX(), 1, -newPoly->getSommets()[0].getY());
                Vecteur_2D interPoint = intersection(linePolygon, iterLine);

                // 如果交点在多边形边上
                if (interPoint.getX() >= min(x1, newPoly->getSommets()[0].getX())
                    && interPoint.getX() <= max(x1, newPoly->getSommets()[0].getX())
                    && interPoint.getY() >= min(y1, newPoly->getSommets()[0].getY())
                    && interPoint.getY() <= max(y1, newPoly->getSommets()[0].getY())) {
                    points.push_back(interPoint); // 将交点保存
                }
            }
            else {
                // 多边形边直线
                linePolygon = linearSolution(x1, 1, -y1, x2, 1, -y2);
                Vecteur_2D interPoint = intersection(linePolygon, iterLine);

                // 如果交点在多边形边上
                if (interPoint.getX() >= min(x1, x2) && interPoint.getX() <= max(x1, x2)
                    && interPoint.getY() >= min(y1, y2) && interPoint.getY() <= max(y1, y2)) {
                    points.push_back(interPoint); // 将交点保存
                }
            }
        }
    }

    double X[points.size()]; // 保存每个航点的x坐标
    for(int i = 0; i < (int)points.size(); i = i+2){
        if(i == 0){ // 第一条线必定不与多边形相交，所以航点的横坐标取第二条线的交点横坐标
            X[0] = points[0].getX();
            X[1] = points[1].getX();
            for(int j = 0; j < newPoly->getNombreSommets(); j++){
                if(points[i].getX() < points[i+1].getX()){ // 第一个点在第二个点的左边
                    if(newPoly->getSommets()[j].getY() < points[i].getY()){ // 有多边形的点在第一个点下面
                        X[i] = min(newPoly->getSommets()[j].getX(),points[i].getX()); // 取最小值
                    }
                    if(newPoly->getSommets()[j].getY() < points[i+1].getY()){ // 有多边形的点在第二个点下面
                        X[i+1] = max(newPoly->getSommets()[j].getX(),points[i+1].getX()); // 取最大值
                    }
                }
                else{ // 第一个点在第二个点右边
                    if(newPoly->getSommets()[j].getY() < points[i].getY()){ // 有多边形的点在第一个点下面
                        X[i] = max(newPoly->getSommets()[j].getX(),points[i].getX()); // 取最大值
                    }
                    if(newPoly->getSommets()[j].getY() < points[i+1].getY()){ // 有多边形的点在第二个点下面
                        X[i+1] = min(newPoly->getSommets()[j].getX(),points[i+1].getX()); // 取最小值
                    }
                }
            }
        }
        else{ // 其他的航点
            if(points[i].getX() < points[i+1].getX()){ // 第一个点在第二个点的左边
                X[i] = min(points[i-2].getX(), points[i-1].getX());
                if(X[i] > points[i].getX())
                    X[i] = points[i].getX();
                X[i+1] = max(points[i-2].getX(), points[i-1].getX());
                if(X[i+1] < points[i+1].getX())
                    X[i+1] = points[i+1].getX();

                for(int j = 0; j < newPoly->getNombreSommets(); j++){ // 查找子区域有没有多边形的顶点
                    if(newPoly->getSommets()[j].getY() < points[i].getY() &&
                            newPoly->getSommets()[j].getY() > points[i-1].getY()){
                        X[i] = min(X[i], newPoly->getSommets()[j].getX());
                        X[i+1] = max(X[i+1], newPoly->getSommets()[j].getX());
                    }
                }
            }
            else{ // 第一个点在第二个点右边
                X[i] = max(points[i-2].getX(), points[i-1].getX());
                if(X[i] < points[i].getX())
                    X[i] = points[i].getX();
                X[i+1] = min(points[i-2].getX(), points[i-1].getX());
                if(X[i+1] > points[i+1].getX())
                    X[i+1] = points[i+1].getX();

                for(int j = 0; j < newPoly->getNombreSommets(); j++){ // 查找子区域有没有多边形的顶点
                    if(newPoly->getSommets()[j].getY() < points[i].getY() &&
                            newPoly->getSommets()[j].getY() > points[i-1].getY()){
                        X[i] = max(X[i], newPoly->getSommets()[j].getX());
                        X[i+1] = min(X[i+1], newPoly->getSommets()[j].getX());
                    }
                }
            }
        }
    }

    // 将航点坐标确定
    for (int i = 0; i < (int)points.size(); i++) {
        points[i].setX(X[i]);
        points[i].setY(points[i].getY() - radiusDrone);
    }

    // 存储航线
    int numberIter = points.size() / 4; // 四个点为一个循环  点1-2-4-3-5
    for(int i = 0; i < numberIter*4; i = i + 4){ // 将前4的倍数的数量的点之间连接成航线
        Vecteur_2D v1 = points[i];
        Vecteur_2D v2 = points[i+1];
        Vecteur_2D v3;
        Vecteur_2D v4;
        if(i != 0){
            if(segments[segments.size()-2][1].getX() < segments[segments.size()-2][0].getX()){ // 上个循环的最后一个点在左边
                if(v1.getX() > v2.getX()){ // 新的循环的第一个点在右边
                    v1 = points[i+1];
                    v2 = points[i];
                }
            }
            else{
                if(v1.getX() < v2.getX()){ // 新的循环的第一个点在左边
                    v1 = points[i+1];
                    v2 = points[i];
                }
            }
        }
        if(v1.getX() < v2.getX()){ // 第一个点在第二个点左边
            if(points[i+3].getX() < points[i+2].getX()){ // 第四个点在第三个点左边，所以二连三
                v3 = points[i+3];
                v4 = points[i+2];
            }
            else{
                v3 = points[i+2];
                v4 = points[i+3];
            }
        }
        else{ // 第一个点在第二个点右边
            if(points[i+3].getX() < points[i+2].getX()){ // 第四个点在第三个点左边，所以二连四
                v3 = points[i+2];
                v4 = points[i+3];
            }
            else{
                v3 = points[i+3];
                v4 = points[i+2];
            }
        }

        // 第一条线--平行线
        vector<Vecteur_2D> segment;
        segment.push_back(v1);
        segment.push_back(v2);
        segments.push_back(segment);
        segment.clear();

        // 第二条线
        segment.push_back(v2);
        segment.push_back(v4);
        segments.push_back(segment);
        segment.clear();

        // 第三条线--平行线
        segment.push_back(v4);
        segment.push_back(v3);
        segments.push_back(segment);
        segment.clear();

        // 第四条线
        if(i+4 < (int)points.size()){ // 还剩一个点
            segment.push_back(v3);

            if(i+5 < (int)points.size()){ // 还剩两个点
                if(v3.getX() < v4.getX()){ // v3在v4左边
                    if(points[i+4].getX() < points[i+5].getX()){ // v5也在v6左边
                        segment.push_back(points[i+4]);
                    }
                    else{
                        segment.push_back(points[i+5]);
                    }
                }
                else{
                    if(points[i+4].getX() > points[i+5].getX()){ // v5也在v6右边
                        segment.push_back(points[i+4]);
                    }
                    else{
                        segment.push_back(points[i+5]);
                    }
                }
            }
            else
                segment.push_back(points[i+4]);

            segments.push_back(segment);
            segment.clear();
        }
    }

    vector<Vecteur_2D> segment;
    segment.push_back(segments[segments.size()-1][1]);
    int rest = points.size() - numberIter*4; // 剩下的点的数量（1-3）
    if(rest >= 2){ // 还有两三个点
        if(segment[0] == points[numberIter*4]) // 航线的最后一个点等于4的倍数+1的航点
            segment.push_back(points[numberIter*4+1]);
        else // 等于4的倍数的航点
            segment.push_back(points[numberIter*4]);
        segments.push_back(segment);
        segment.clear();
        if(rest == 3){ // 还有三个点
            segment.push_back(segments[segments.size()-1][1]); // 上一个点
            if(segment[0] == points[points.size()-1])
                segment.push_back(points[points.size()-2]);
            else
                segment.push_back(points[points.size()-1]);
            segments.push_back(segment);
            segment.clear();
        }
    }

    for(int i = 0; i < (int)points.size(); i++)
        points[i] = toOriginCoord(points[i], angle);
    for(int i = 0; i < (int)segments.size(); i++){
        segments[i][0] = toOriginCoord(segments[i][0], angle);
        segments[i][1] = toOriginCoord(segments[i][1], angle);
    }
    return segments;
}

