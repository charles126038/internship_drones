#ifndef NAVIGATION_ALGORITHMS_H
#define NAVIGATION_ALGORITHMS_H

#include "Polygone.h"
#include <vector>
#include <Eigen/Eigen/Eigen>

using namespace std;
using namespace Eigen;

/**
 * @brief The Navigation class 无人机航线规划算法
 * 通过courseAngles.txt文件输入需要计算的航向角，并自动转换为弧度，计算路线，比较不同航线角得出的结果
 */
class Navigation{
private:
    vector<double> courseAngles; // 航向角
    double angleToRadian(int); // 角度转换
    Polygone *polygone;
    double radiusDrone; // 航拍半径
    vector<Vecteur_2D> points; // 航点
    Vector2d line; // 直线表达式(a, -1, c)
    vector<vector<Vecteur_2D>> segmentsNavigation; // 平行线航线线段
    double bestAngle; // 最佳航向角
    double rateRedundant; // 多余覆盖率

    /**
     * @brief linearSolution 解二元直线方程(高斯消元) ax+by+c=0 (ax-y+c=0)
     * @param a1 直线一a
     * @param b1 直线一b
     * @param c1 直线一c
     * @param a2 直线二a
     * @param b2 直线二b
     * @param c2 直线二c
     * @return vector2d
     */
    Vector2d linearSolution(double a1, double b1, double c1, double a2, double b2, double c2);

    /**
     * @brief heuristicCalculate 求工作区域多余覆盖率，越小越优
     * @return
     */
    double heuristicCalculate(vector<vector<Vecteur_2D>> segmentsNavigation);

public:
    const double PI = 3.1415926;
    /**
     * @brief Navigation 读取文件获取需要计算的航向角
     * @param polygone 多边形
     * 文件：:/mapsInfo/courseAngles.txt
     * 格式：每个角度用换行符隔开
     */
    Navigation(Polygone *polygone);

    /**
     * @brief algorithm 主算法
     */
    void algorithm();

    /**
     * @brief originCoordRotation 将点换算到新坐标系
     * @param v
     * @param angle 航向角
     * @return
     */
    Vecteur_2D originCoordRotation(const Vecteur_2D& v, const int& angle);
    /**
     * @brief toOriginCoord 将点换算回原坐标系
     * @param v
     * @param angle
     * @return
     */
    Vecteur_2D toOriginCoord(const Vecteur_2D& v, const int& angle);

    /**
     * @brief intersection 矩阵求两条直线的交点 ax-y+c=0
     * @param line1 直线一 a1x+b1y+c1=0
     * @param line2 直线二 a2x+b2y+c2=0
     * @return 交点Vecteur_2D
     */
    Vecteur_2D intersection(Vector2d line1, Vector2d line2);

    /**
     * @brief navigationPoints 求航点，将多边形放入新坐标系，求出各航点并将航点新坐标转换回原直角坐标系
     * @param angle 航向角
     * @return
     */
    vector<vector<Vecteur_2D>> navigationPoints(const int& angle);

    vector<vector<Vecteur_2D>> getSegments() {return segmentsNavigation;}

    Vector2d getLine() {return line;}

    double getBestAngle() {return bestAngle;}

    vector<Vecteur_2D> getPoints() {return points;}

    double getRateRebudant() {return rateRedundant;}
};





#endif // NAVIGATION_ALGORITHMS_H
